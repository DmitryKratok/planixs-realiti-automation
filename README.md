# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### How do I get API requests work? ###

You need to forward a port to a backend that will be queried 
(or just run tests on a machine with backend).
E.g. 9000 port is needed for hubapiv1 endpoints 
(you can check what port is used in etl_config.yaml)

Note: request will be executed on a server from which port has
been forwarded, not from the server in `base_url`.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact