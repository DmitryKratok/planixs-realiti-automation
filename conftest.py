import logging
import os
import pytest
from src.core.browser import Browser
from src.utils.rabbitmq import RabbitRPCClient
from src.utils.yaml_helper import get_config
from src.utils.screenshots import take_screenshot, get_screenshot_dir
from src.db.db_helper import HubDbClient, RecsDbClient

log = logging.getLogger(__name__)


@pytest.fixture(scope="class")
def driver_init(request):
    test_name = os.environ.get('PYTEST_CURRENT_TEST') \
        .split(':')[-1].split(' ')[0]
    log.info(f'Started test: {test_name}')
    driver = Browser.get_driver()
    request.cls.driver = driver
    yield
    HubDbClient().close()
    RecsDbClient().close()
    RabbitRPCClient().close_connection()
    driver.close()


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    if rep.when == 'call':
        if item.instance.driver:
            web_driver = item.instance.driver
    else:
        # This test does not use web_driver and we do need screenshot for it
        return
    if rep.failed:
        log.info(f'Test failed: {item.name}')
        if get_config().screenshots.if_failed:
            take_screenshot(web_driver, f'{item.nodeid}_FAILED')
    else:
        log.info(f'Test successful: {item.name}')
        if get_config().screenshots.if_passed:
            take_screenshot(web_driver, f'{item.nodeid}_PASSED')


def pytest_configure(config):
    """Initial checks of configuration before all tests"""
    screenshot_dir = get_screenshot_dir()
    if screenshot_dir is None:
        raise ValueError(
            "No valid path found for screenshot directory. Either set "
            "the path in config or "
            "set the SCREENSHOT_DIR environment variable.")
