from datetime import datetime


class Base:

    id: str
    created_at: str
    modified_at: str

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

    def __getattr__(self, item):
        return None

    def _to_datetime(self, value: str) -> datetime:
        if ":" == value[-3]:
            value = value[:-3] + value[-2:]
        return datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%f%z')

    def get_modified_at_dt(self):
        return self._to_datetime(self.modified_at)

    def get_created_at_dt(self):
        return self._to_datetime(self.created_at)
