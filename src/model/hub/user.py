from src.model.Base import Base


class User(Base):

    username: str
    email: str
    password: str

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
