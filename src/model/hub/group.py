from src.model.Base import Base


class Group(Base):

    def __init__(self, name=None, group_type=None, **kwargs):
        """
        :param name: Name of the group
        :param group_type: Type of the group.
        Could be either `functional` or `data`
        """
        self.name = name
        self.group_type = group_type
        super().__init__(**kwargs)
