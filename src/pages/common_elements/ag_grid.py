from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from src.pages.basePage import BasePage


class AgGrid(BasePage):

    def __init__(self, driver, *args, is_popup=False):
        """
        :param driver: WebDriver object
        :param args: Headers of a table
        :param: A flag that indicates if this grid is located inside a popup
        """
        super().__init__(driver)
        columns_list = list(args)
        if is_popup:
            self._grid = self.find_element(
                (By.XPATH, "(//div[@role='grid'])[2]")
            )
            self._pagination = self.find_element(
                (By.CSS_SELECTOR, "div.ag-paging-panel:nth-of-type(2)")
            )
        else:
            self._grid = self.find_element(
                (By.XPATH, "//div[@role='grid']")
            )
            self._pagination = self.find_element(
                (By.CSS_SELECTOR, "div.ag-paging-panel")
            )
        self.columns = {
            col: self.__get_column_id(col) for col in columns_list
        }

    def __get_column_locator(self, name):
        return f"//div[contains(concat(' ', @class, ' '), " \
               f"' ag-header-cell ') and descendant::text()='{name}']"

    def __get_row_by_column_value(self, column_name, value) -> WebElement:
        column_id = self.columns.get(column_name)
        return self.find_element(
            (By.XPATH,
             f".//div[contains(@role, 'row') "
             f"and descendant::div[@col-id='{column_id}' "
             f"and (descendant::text()='{value}' or text()='{value}')]]"),
            root_element=self._grid
        )

    def __get_column_id(self, name):
        return self.find_element(
            (By.XPATH, self.__get_column_locator(name)),
            root_element=self._grid
        ).get_attribute('col-id')

    def select_row_with_value(self, column_name, value):
        row = self.__get_row_by_column_value(column_name, value)
        row.find_element(
            By.XPATH,
            ".//span[contains(@class, 'ag-selection-checkbox')]"
        ).click()

    def click_on_row_with_value(self, column_name, value):
        self.__get_row_by_column_value(column_name, value).click()

    def get_row_values(self, column_name, value):
        row = self.__get_row_by_column_value(column_name, value)
        return self.__extract_values_from_row(row)

    def get_rows_values(self):
        rows = self.find_elements(
            (By.XPATH,
             ".//div[contains(@class, 'ag-center-cols-clipper')]//"
             "div[contains(concat(' ', @class, ' '), ' ag-row ')]"),
            root_element=self._grid
        )
        result = []
        for row in rows:
            row_dict = self.__extract_values_from_row(row)
            result.append(row_dict)
        return result

    def __extract_values_from_row(self, row):
        row_dict = {}
        for column in self.columns:
            row_dict[column] = self.find_element(
                (
                    By.XPATH,
                    f"(./div[contains(@class, 'ag-cell') "
                    f"and @col-id='{self.columns.get(column)}'])"
                ),
                root_element=row
            ).text
        return row_dict

    def get_total_records(self):
        self.wait_until_ag_page_loads()
        return self.find_element(
            (By.CSS_SELECTOR, "span[ref='lbRecordCount']"),
            root_element=self._pagination
        ).text

    def get_number_of_current_page(self):
        self.wait_until_ag_page_loads()
        return self.find_element(
            (By.CSS_SELECTOR, "span[ref='lbCurrent']"),
            root_element=self._pagination
        ).text

    def get_total_pages(self):
        self.wait_until_ag_page_loads()
        return self.find_element(
            (By.CSS_SELECTOR, "span[ref='lbTotal']"),
            root_element=self._pagination
        ).text
