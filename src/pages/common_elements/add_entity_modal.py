from selenium.common.exceptions import UnexpectedTagNameException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from src.pages.common_elements.checkbox import Checkbox


class Modal:

    def __init__(self, element: WebElement):
        """Constructor for a modal view
        with ability to add entities to a parent
        :param element: WebElement with attribute `role`='dialog'"""

        if 'dialog' not in element.get_attribute('role'):
            raise UnexpectedTagNameException("Unsupported web element.")

        self._el = element

    def select_all(self):
        input_element = self._el.find_element(
            (By.XPATH, "//div[contains(@class, "
                       "'MuiCardActions-root')]//input")
        )
        Checkbox(input_element).check()

    def add_selected(self):
        self._el.find_element(
            By.XPATH,
            f"//button[descendant::text()='Add']"
        ).click()
