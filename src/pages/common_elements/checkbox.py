import time
from selenium.common.exceptions import UnexpectedTagNameException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement


class Checkbox:

    def __init__(self, webelement: WebElement):
        """
        Constructor.
        A check is made that the given element is a checkbox.
        If it is not, then an UnexpectedTagNameException is thrown.
        """
        # The thought about this is - we'll switch to react by the time
        # we'll implement a tests for Realiti screens with checkboxes
        if 'checkbox' in webelement.get_attribute('role'):
            raise NotImplementedError(
                'Realiti checkbox type is not implemented yet.'
            )
        if 'checkbox' not in webelement.get_attribute('type'):
            raise UnexpectedTagNameException(
                'Expecting a checkbox button element!'
            )

        self._el = webelement.find_element(By.XPATH, "./ancestor::node()[2]")

    def is_checked(self) -> bool:
        """Determines if a checkbox element is checked.
        Returns True if the element is checked.
        Returns False if the element is not checked.
        """
        return 'Mui-checked' in self._el.get_attribute('class')

    def is_interminate(self) -> bool:
        """Determines if a checkbox element is semi-checked
        (for Select All checkboxes when some of grid rows are selected).
        Returns True if the element is semi-checked.
        Returns False otherwise.
        """
        return 'MuiCheckbox-indeterminate' in self._el.get_attribute('class')

    def check(self):
        """ If a checkbox is not checked then check it,
        otherwise leave as is """
        if not self.is_checked():
            self._el.click()

    def uncheck(self):
        """ If a checkbox is checked then uncheck it,
        otherwise leave as is """
        if self.is_checked():
            self._el.click()
        elif self.is_interminate():
            self._el.click()
            # Hacky thing. Checkbox jumps between states when semi-checked
            # which makes it really tricky to track it properly.
            time.sleep(0.01)
            self._el.click()
