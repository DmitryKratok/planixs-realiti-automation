"""Header page object"""
from selenium.webdriver.common.by import By

from src.pages import LoginPage
from src.pages.basePage import BasePage


class Header(BasePage):

    __popup_xpath = "//div[@data-test-classes='classes-modal']"
    __welcome = (By.CSS_SELECTOR, "header p")
    __kebab_menu = (By.CSS_SELECTOR, "button[title=\"Menu\"]")
    __reload_permissions = (By.XPATH, "//ul/li[text() = 'Reload user data']")
    __logout = (By.XPATH, "//ul/li[text() = 'Log out']")
    __version_button = (By.XPATH, "//ul/li[text() = 'Version info']")
    __version = (By.XPATH, f"{__popup_xpath}//p")
    __popup_close_button = (By.XPATH, f"{__popup_xpath}//button")

    def get_header_welcome_text(self):
        return self.find_element(self.__welcome).text

    def _click_kebab(self):
        self.click_element(self.__kebab_menu)

    def _open_version_popup(self):
        self._click_kebab()
        self.click_element(self.__version_button)

    def _close_version_popup(self):
        self.click_element(self.__popup_close_button)

    def get_version(self):
        self._open_version_popup()
        version_str = self.find_element(self.__version).text
        self._close_version_popup()
        return version_str.split(': ')[1]

    def reload_permissions(self):
        """Reloads user permissions"""
        self._click_kebab()
        self.click_element(self.__reload_permissions)
        return self

    def logout(self) -> LoginPage:
        self._click_kebab()
        self.click_element(self.__logout)
        return LoginPage(self.driver)
