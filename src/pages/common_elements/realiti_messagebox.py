from selenium.common.exceptions import UnexpectedTagNameException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement


class RealitiMessagebox:

    def __init__(self, webelement: WebElement):
        """
        Constructor.
        A check is made that the given element is, indeed, a message box tag.
        If it is not, then an UnexpectedTagNameException is thrown.
        """
        if 'x-message-box' not in webelement.get_attribute('class'):
            raise UnexpectedTagNameException(
                f'RealitiMessagebox only works on <select> elements, '
                f'not on {webelement.tag_name}'
                )

        self._el = webelement

    @property
    def text(self) -> str:
        return self._el.find_element(By.CSS_SELECTOR,
                                     "div[role = 'textbox']").text

    @property
    def header(self) -> str:
        return self._el.find_element(By.CSS_SELECTOR,
                                     "span.x-header-text").text

    def click_x_button(self):
        self._el.find_element("img.x-tool-close").click()

    def click_ok_button(self):
        self._el.find_element(By.XPATH,
                              "//a[descendant::span[text() = 'OK']]").click()
