from selenium.webdriver.common.by import By
from src.pages.basePage import BasePage


class CentralNav(BasePage):

    def __get_menu_locator(self, name):
        return f"//li[descendant::text()='{name}']"

    def select_sidemenu(self, sidemenu_path):
        """
        :param sidemenu_path: string
        example: "Admin->Labels->Global Labels"
        """

        xpath = "//ul"
        sidemenu_list = sidemenu_path.split("->")
        for i, item in enumerate(sidemenu_list, 1):
            xpath += self.__get_menu_locator(item)
            if i != len(sidemenu_list):
                if "entered" not in self. \
                        find_element((By.XPATH, f"{xpath}/div")). \
                        get_attribute('class'):
                    self.click_element((By.XPATH, xpath))
            else:
                self.click_element((By.XPATH, xpath))
