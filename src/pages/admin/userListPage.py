from selenium.webdriver.common.by import By
from src.pages.basePage import BasePage
from src.pages.common_elements.ag_grid import AgGrid
from src.pages.common_elements.checkbox import Checkbox


class UserListPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    __check_all = (By.XPATH, "//div[contains(@class, "
                             "'MuiCardActions-root')]//input")

    def get_grid(self):
        return AgGrid(self.driver, 'Username', 'Email', 'First name',
                      'Last name', 'Active', 'Superuser')

    def click_create_button(self):
        btn_locator = self.get_button_with_text('Create')
        self.click_element(btn_locator)

    def select_all_users(self):
        checkbox = Checkbox(self.find_element(self.__check_all))
        checkbox.check()

    def unselect_all_users(self):
        checkbox = Checkbox(self.find_element(self.__check_all))
        checkbox.uncheck()
