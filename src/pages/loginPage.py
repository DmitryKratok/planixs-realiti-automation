from selenium.webdriver.common.by import By
from src.pages.basePage import BasePage
from src.utils.config_manager import get_config


class LoginPage(BasePage):

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    __signin_button = (By.CLASS_NAME, "button-primary")
    __user_name_input = (By.ID, "username")
    __password_input = (By.ID, "password")

    def click_login_button(self):
        self.click_element(self.__signin_button)

    def input_username(self, value):
        self.type_text(self.__user_name_input, value)

    def input_password(self, value):
        self.type_text(self.__password_input, value)

    def navigate_to_login_page(self):
        self.go_to_url(
            f"{get_config().base_url}:{get_config().port_web}")
