from selenium.webdriver.common.by import By
from src.pages.basePage import BasePage


class HomePage(BasePage):

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    __welcome_header = By.XPATH, "//span[contains(text()," \
                                 "'Welcome to Realiti,')]"

    __welcome_note_1 = By.XPATH, "//div/p[contains(text(), 'Realiti is"\
                                 " a complete suite of applications" \
                                 " built to deliver insight over your" \
                                 " treasury and cash management" \
                                 " business.')]"

    __welcome_note_2 = By.XPATH, "//div/p[contains(text(), 'Use the" \
                                 " left hand menu to navigate around" \
                                 " the suite and its applications.')]"

    __no_access_note_1 = By.XPATH, "//div/p[contains(text(), 'You do" \
                                   " not have permission to access any" \
                                   " of the supported applications.')]"

    __no_access_note_2 = By.XPATH, "//div/p[contains(text(), 'Please " \
                                   "contact your system administrator.')]"


    def is_header_visible(self):
        return self.is_element_visible(self.__welcome_header)

    def is_note1_visible(self):
        return self.is_element_visible(self.__welcome_note_1)

    def is_note2_visible(self):
        return self.is_element_visible(self.__welcome_note_2)

    def is_no_access_note1_visible(self):
        return self.is_element_visible(self.__no_access_note_1)

    def is_no_access_note2_visible(self):
        return self.is_element_visible(self.__no_access_note_2)

    def get_welcome_header_text(self):
        return self.get_text(self.__welcome_header)
