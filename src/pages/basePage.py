import logging
import platform
from typing import List, Tuple

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.keys import Keys
from src.utils.decorators import log_exception


class BasePage:

    log = logging.getLogger(__name__)

    def __init__(self, driver):
        self.driver = driver

    @log_exception("Can't find element by locator: {}")
    def find_element(
            self, locator: Tuple, time=10, root_element: WebElement = None
    ) -> WebElement:
        """
        :param locator: locator tuple
        :param time: wait time. 10 by default
        :param root_element: Element from what search should be started.
        If not provided, DOM's root will be used
        """
        self.log.debug(f'Trying to find element by locator: {locator}')
        if root_element:
            return WebDriverWait(root_element, time) \
                .until(EC.presence_of_element_located(locator),
                       message=f"Can't find element by locator: {locator}")
        return WebDriverWait(self.driver, time) \
            .until(EC.presence_of_element_located(locator),
                   message=f"Can't find element by locator: {locator}")

    @log_exception("Can't find elements by locator: {}")
    def find_elements(
            self, locator: Tuple, time=10, root_element: WebElement = None
    ) -> List[WebElement]:
        """
        :param locator: locator tuple
        :param time: wait time. 10 by default
        :param root_element: Element from what search should be started.
        If not provided, DOM's root will be used
        """
        self.log.debug(f'Trying to find elements by locator: {locator}')
        if root_element:
            return WebDriverWait(root_element, time) \
                .until(EC.presence_of_all_elements_located(locator),
                       message=f"Can't find elements by locator: {locator}")
        return WebDriverWait(self.driver, time) \
            .until(EC.presence_of_all_elements_located(locator),
                   message=f"Can't find elements by locator: {locator}")

    def get_button_with_text(self, text, contains=False) -> Tuple:
        if contains:
            return (By.XPATH,
                    f"//button[descendant::*[text()[contains(.,'{text}')]]]")
        else:
            return By.XPATH, f"//button[descendant::*[text()='{text}']]"

    @log_exception('Failed to click web element with locator: {}')
    def click_element(self, locator: Tuple, time=10):
        self.log.info(f'Clicking web element with locator: {locator}')
        WebDriverWait(self.driver, time)\
            .until(EC.element_to_be_clickable(locator)).click()

    def js_click(self, element):
        self.driver.execute_script("arguments[0].click();", element)

    def type_text(self, locator: Tuple, text):
        input_field = self.find_element(locator)
        self.log.debug(f"Typing text '{text}' "
                       f"to a field with locator: {locator}")
        if platform.system() == 'Darwin':   # mac os
            input_field.clear()
        else:                               # others
            input_field.send_keys(Keys.CONTROL, 'a')
        input_field.send_keys(text)

    def get_text(self, locator) -> str:
        return self.find_element(locator).text

    def is_element_visible(self, locator) -> bool:
        is_visible = self.find_element(locator).is_displayed()
        if not is_visible:
            self.log.debug(f"Element is not visible: {locator}")
        return is_visible

    def check_attribute_value(self, locator, attribute_name, value):
        return self.find_element(locator)\
                   .get_attribute(attribute_name) == value

    def wait_until_element_invisible(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(
            EC.invisibility_of_element_located(locator)
        )

    def wait_until_ag_page_loads(self):
        self.wait_until_element_invisible((By.CSS_SELECTOR, "div.ag-overlay"))

    def go_to_url(self, url):
        self.log.info(f'Opening URL: {url}')
        return self.driver.get(url)

    def get_current_url(self):
        current_url = self.driver.current_url
        self.log.info(f'Getting current URL: {current_url}')
        return current_url
