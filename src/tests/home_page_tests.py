import os
from src.steps.loginPageSteps import LoginPageSteps
from src.steps.homePageSteps import HomePageSteps
from src.tests.basicTest import BasicTest
from src.utils.common import get_root
from src.utils.yaml_helper import get_yaml_data


class TestURL(BasicTest):

    def test_verify_home_page_ui(self):
        LoginPageSteps(self.driver).login()
        HomePageSteps(self.driver).verify_home_page_url()
        HomePageSteps(self.driver).verify_home_page_welcome_notes()

    def test_verify_home_page_no_permissions(self):
        users_data = get_yaml_data(os.path.join(get_root(),
                                                "src/test_data/users.yaml"))
        no_access_user = users_data.get("wop_user").get("username")
        no_access_pw = users_data.get("wop_user").get("password")
        LoginPageSteps(self.driver).login(no_access_user, no_access_pw)
        HomePageSteps(self.driver).verify_home_page_url()
        HomePageSteps(self.driver).verify_home_page_no_permissions_notes()
