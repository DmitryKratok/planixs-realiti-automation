import logging
from src.utils.config_manager import get_config


class BasicSteps:

    def __init__(self, driver):
        self.driver = driver
        self.log = logging.getLogger(__name__)

    def get_basic_web_url(self):
        return f"{get_config().base_url}" \
               f":{get_config().port_web}/"

    def get_expected_displayed_username(self, user=None,
                                        firstname=None, lastname=None):
        username, first_name, last_name = user, firstname, lastname
        if not username:
            username, first_name, last_name = get_config().username,\
            get_config().firstname, get_config().lastname
        if not first_name:
            return username
        else:
            if last_name:
                return f"{first_name} {last_name} ({username})"
            else:
                return f"{first_name} ({username})"
