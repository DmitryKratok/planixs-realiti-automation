import os

from src.pages.homePage import HomePage
from src.steps.basicSteps import BasicSteps
from src.utils.common import get_root
from src.utils.yaml_helper import get_yaml_data


class HomePageSteps(BasicSteps):

    def __init__(self, driver):
        BasicSteps.__init__(self, driver)
        self.home_page = HomePage(driver)

    def verify_home_page_welcome_notes(self):
        expected_ui_username = self.get_expected_displayed_username()
        welcome_text = self.home_page.get_welcome_header_text()
        assert self.home_page.is_header_visible(), \
            "Welcome header is not visible on the Home page"
        assert welcome_text.endswith(expected_ui_username), \
            f"Incorrect displayed username. Expected: " \
            f"{expected_ui_username}. Actual " \
            f"{welcome_text.split(', ')[1]}"
        assert self.home_page.is_note1_visible(), \
            "Welcome note 1 is not visible on the Home page"
        assert self.home_page.is_note2_visible(), \
            "Welcome note 2 is not visible on the Home page"

    def verify_home_page_no_permissions_notes(self):
        users_data = get_yaml_data(
            os.path.join(get_root(), "src/test_data/users.yaml"))
        expected_ui_username = self.get_expected_displayed_username(
            users_data.get("wop_user").get("username"),
            users_data.get("wop_user").get("firstname"),
            users_data.get("wop_user").get("lastname"))
        welcome_text = self.home_page.get_welcome_header_text()
        assert self.home_page.is_header_visible(), \
            "Welcome header is not visible on the Home page"
        assert welcome_text.endswith(expected_ui_username), \
            f"Incorrect displayed username. Expected: " \
            f"{expected_ui_username}. Actual " \
            f"{welcome_text.split(', ')[1]}"
        assert self.home_page.is_no_access_note1_visible(), \
            "Welcome note 1 for a user without access is not visible on the " \
            "Home page"
        assert self.home_page.is_no_access_note2_visible(), \
            "Welcome note 2 for a user without access is not visible on the " \
            "Home page"

    def verify_home_page_url(self):
        expected_url = self.get_basic_web_url()
        actual_url = self.get_current_web_url()
        assert actual_url == expected_url, \
            f"Home page URL is incorrect. Expected: {expected_url}." \
            f" Actual: {actual_url}"

    def get_current_web_url(self):
        return self.home_page.get_current_url()
