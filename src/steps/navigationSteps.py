from src.pages.common_elements.central_navigation import CentralNav
from src.steps.basicSteps import BasicSteps


class NavigationSteps(BasicSteps):

    def __init__(self, driver):
        super().__init__(driver)
        self.nav = CentralNav(self.driver)

    def navigate_to_users_list(self):
        self.nav.select_sidemenu('Admin->Users')
