from src.pages.admin.userListPage import UserListPage
from src.steps.basicSteps import BasicSteps


class UserListViewSteps(BasicSteps):

    def __init__(self, driver):
        super().__init__(driver)
        self.list_view = UserListPage(self.driver)

    def select_all_users(self):
        self.list_view.select_all_users()

    def unselect_all_users(self):
        self.list_view.unselect_all_users()

    def open_user_by_username(self, username):
        self.list_view.get_grid().click_on_row_with_value('Username', username)
