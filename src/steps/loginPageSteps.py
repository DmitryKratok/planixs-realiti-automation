from src.pages.loginPage import LoginPage
from src.utils.config_manager import get_config
from src.steps.basicSteps import BasicSteps


class LoginPageSteps(BasicSteps):

    def __init__(self, driver):
        BasicSteps.__init__(self, driver)
        self.login_page = LoginPage(driver)
        self.username = get_config().username
        self.password = get_config().password

    def login(self, user=None, pwd=None):
        self.login_page.navigate_to_login_page()
        username = user if user else self.username
        password = pwd if pwd else self.password
        self.log.info(f"Logging in with credentials: "
                      f"{username}/{password}")
        self.login_page.input_username(username)
        self.login_page.input_password(password)
        self.login_page.click_login_button()
