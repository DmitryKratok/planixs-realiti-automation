from src.pages.common_elements.header import Header
from src.steps.basicSteps import BasicSteps
from src.utils.yaml_helper import get_config


class HeaderSteps(BasicSteps):

    def __init__(self, driver):
        super().__init__(driver)
        self.header = Header(self.driver)

    def reload_permissions(self):
        self.header.reload_permissions()

    def logout(self):
        self.header.logout()

    def validate_version(self):
        expected_version = get_config().version
        actual_version = self.header.get_version()
        assert expected_version == actual_version, \
            f"Version is incorrect. " \
            f"Expected version {expected_version}, actual {actual_version}"
