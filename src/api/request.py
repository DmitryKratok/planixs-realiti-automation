from dataclasses import dataclass
import requests

from src.utils.yaml_helper import get_config


@dataclass
class Response:
    status_code: int
    text: str
    as_dict: dict
    headers: dict


class APIRequest:

    def __init__(self, user_id):
        """Initiate a requests session.
        :param user_id: User ID as whom request session will be started"""
        origin = f'{get_config().base_url}:{get_config().port_web}'
        self.headers = {"content-type": "application/json",
                        "Accept-Encoding": "gzip, deflate, br",
                        "X-Hub-API-Key": "ejG5BM02PA_c1lndHsfREjqrW7ljvBoj",
                        "X-Hub-User": user_id,
                        "X-Hub-Auth": "",
                        "Origin": origin}

    def get(self, url) -> Response:
        response = requests.get(url, headers=self.headers)
        return self.__get_response(response)

    def post(self, url, payload, headers=None) -> Response:
        if headers:
            headers.update(self.headers)
        else:
            headers = self.headers
        response = requests.post(url, data=payload, headers=headers)
        return self.__get_response(response)

    def put(self, url, payload, headers) -> Response:
        headers.update(self.headers)
        response = requests.put(url, data=payload, headers=headers)
        return self.__get_response(response)

    def delete(self, url) -> Response:
        response = requests.delete(url, headers=self.headers)
        return self.__get_response(response)

    def __get_response(self, response) -> Response:
        status_code = response.status_code
        text = response.text

        try:
            as_dict = response.json()
        except Exception:
            as_dict = {}

        headers = response.headers

        return Response(
            status_code, text, as_dict, headers
        )
