import json
from typing import List
from py.error import Error
from src.model.hub.group import Group
from src.api.hub import base_url
from src.model.hub.user import User


def get_group_list(api_session, page=1, per_page=50):
    """Returns a list of Group objects"""
    response = api_session.get(f'{base_url}/groups/'
                               f'?page={page}&per_page={per_page}')
    groups = []
    for group in response.as_dict.get('data'):
        groups.append(Group(**group))
    return groups


def create_group(api_session, group: Group) -> Group:
    """Creates a group via POST request from a Group object.
    :param api_session: Requests session to run request against
    :param group: A Group object from which new group will be created.
        `name` and `group_type` attributes are mandatory for this object
        `group_type` options: `functional`, `data`
        """
    if group.group_type not in ('functional', 'data'):
        raise ValueError('Unsupported group type.')
    payload = json.dumps(group.__dict__)
    response = api_session.post(f'{base_url}/groups/', payload)
    if response.status_code == 201:
        return Group(**response.as_dict.get('data'))
    else:
        raise Error(f"Error creating a group. "
                    f"Response content: {response.as_dict}")


def add_users_to_group(api_session, group: Group, users: List[User]):
    """Adding users to a group
    :param api_session: Requests session to run request against
    :param group: A group object. Should have at least `id` property
    :param users: A list of User objects. Should have at least `id` property
    """
    payload = json.dumps({"batch": [{'id': usr.id} for usr in users]})
    response = api_session.post(
        f'{base_url}/batch/groups/{group.id}/users/', payload
    )
    if response.status_code != 201:
        raise Error(f"Error adding users to a group. "
                    f"Response content: {response.as_dict}")


def add_func_permissions_to_group(api_session, group: Group,
                                  permissions: List[str]):
    """Adding functional permissions to a group
    :param api_session: Requests session to run request against
    :param group: A group object. Should have at least `id` property
    :param permissions: A list of permission ids
    """
    payload = json.dumps({"batch": [{'id': perm} for perm in permissions]})
    response = api_session.post(
        f'{base_url}/batch/groups/{group.id}/permissions/',
        payload
    )
    if response.status_code != 201:
        raise Error(f"Error adding permissions to a group. "
                    f"Response content: {response.as_dict}")


def add_accounts_to_group(api_session, group: Group, accounts: List[str]):
    """Adding accounts to a group
    :param api_session: Requests session to run request against
    :param group: A group object. Should have at least `id` property
    :param accounts: A list of account ids
    """
    payload = json.dumps({"batch": [{'id': acc} for acc in accounts]})
    response = api_session.post(
        f'{base_url}/batch/groups/{group.id}/nostros/',
        payload
    )
    if response.status_code != 201:
        raise Error(f"Error adding account to a group. "
                    f"Response content: {response.as_dict}")
