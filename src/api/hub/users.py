import json
from py.error import Error
from src.model.hub.user import User
from src.api.hub import base_url


def get_user_list(api_session, page=1, per_page=50):
    """Returns a list of User objects"""
    response = api_session.get(f'{base_url}/users/'
                               f'?page={page}&per_page={per_page}')
    users = []
    for user in response.as_dict.get('data'):
        users.append(User(**user))
    return users


def create_user(api_session, user: User) -> User:
    """Creates a user via POST request from a User object.
    :param api_session: Requests session to run request against
    :param user: A User object from which new user will be created.
        `username`, `email` and `is_superuser` attributes are mandatory"""
    payload = json.dumps(user.__dict__)
    response = api_session.post(f'{base_url}/users/', payload)
    if response.status_code == 201:
        return User(**response.as_dict.get('data'))
    else:
        raise Error(f"Error creating a user. "
                    f"Response content: {response.as_dict}")
