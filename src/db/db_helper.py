import logging
import psycopg2
from src.utils.config_manager import get_config
from src.utils.decorators import log_exception


class SQLClient:

    log = logging.getLogger(__name__)

    def __init__(self, connection):
        self.__connection = connection

    @log_exception('Error querying DB: {}')
    def get_one(self, query=None, query_file=None, params=None):
        cursor = self.__connection.cursor()
        if query:
            cursor.execute(query, params)
        elif query_file:
            with open(query_file, 'r') as f:
                cursor.execute(f.read(), params)
        result = cursor.fetchone()
        cursor.close()
        return result

    @log_exception('Error querying DB: {}')
    def get_all(self, query=None, query_file=None, params=None):
        cursor = self.__connection.cursor()
        if query:
            cursor.execute(query, params)
        elif query_file:
            with open(query_file, 'r') as f:
                cursor.execute(f.read(), params)
        result = cursor.fetchall()
        cursor.close()
        return result

    @log_exception('Exception occurred while executing query: {}')
    def execute_query(self, query=None, query_file=None, params=None):
        cursor = self.__connection.cursor()
        if query:
            cursor.execute(query, params)
        elif query_file:
            with open(query_file, 'r') as f:
                cursor.execute(f.read(), params)
        self.__connection.commit()
        cursor.close()

    def close(self):
        try:
            self.__connection.close()
        except AttributeError:
            self.log.debug('No connection to close. Skipping.')


class HubDbClient(SQLClient):
    __connection = None

    def __init__(self):
        if self.__connection is None:
            try:
                params = get_config().hub.db
                self.__connection = psycopg2.connect(**params)
                super().__init__(self.__connection)
            except (Exception, psycopg2.DatabaseError) as error:
                self.log.error(error)


class RecsDbClient(SQLClient):
    __connection = None

    def __init__(self):
        if self.__connection is None:
            try:
                params = get_config().realiti.db
                self.__connection = psycopg2.connect(**params)
                super().__init__(self.__connection)
            except (Exception, psycopg2.DatabaseError) as error:
                self.log.error(error)
