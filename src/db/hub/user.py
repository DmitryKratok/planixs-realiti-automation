import uuid
import bcrypt as bcrypt
from src.model.hub.user import User
from src.db.hub import db

NAMESPACE = uuid.UUID("4b74b739-4a65-40c0-9524-f393d9ef6654")


def create_user_in_db(user: User):
    """Hacky way of creating a user.
    :param user: User object. `username`, `email` and `password` attributes
    are required for this to work
    :returns A User object"""
    if not user.id:
        user.id = _generate_id(user.username + user.email)
    if not user.is_superuser:
        user.is_superuser = True
    hashed_pw = str(bcrypt.hashpw(user.password.encode(),
                                  bcrypt.gensalt()))[2:-1]

    query = '''INSERT INTO "user" (id, username, password, is_superuser, 
    is_active, email, first_name, last_name)  
    VALUES (%s, %s, %s, %s, true, %s, %s, %s) 
    ON CONFLICT (id) DO UPDATE SET (password, is_superuser) = 
    (EXCLUDED.password, EXCLUDED.is_superuser);'''

    db.execute_query(
        query, params=(user.id, user.username, hashed_pw, user.is_superuser,
                       user.email, user.first_name, user.last_name)
    )
    return user


def _generate_id(value, namespace=NAMESPACE):
    """Generate and return a deterministic id for the input value."""
    return str(uuid.uuid5(namespace, str(value)))
