from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait
from src.utils.config_manager import get_config
from webdriver_manager import chrome, firefox
from webdriver_manager.microsoft import \
    IEDriverManager, EdgeChromiumDriverManager


class Browser:

    @staticmethod
    def accept_untrusted_certificates(browser_type):
        if browser_type == "firefox":
            profile = webdriver.FirefoxProfile()
            profile.accept_untrusted_certs = True
            return profile
        elif browser_type == "ie":
            capabilities = webdriver.DesiredCapabilities().INTERNETEXPLORER
            capabilities['acceptSslCerts'] = True
        elif browser_type == "edge":
            capabilities = webdriver.DesiredCapabilities().EDGE
            capabilities['acceptSslCerts'] = True
            return capabilities
        else:
            options = webdriver.ChromeOptions()
            options.add_argument('ignore-certificate-errors')
            return options

    @staticmethod
    def manage_resolution():

        window_width = get_config().browser_resolution.width
        window_height = get_config().browser_resolution.height
        if type(window_width) != int or type(window_height) != int \
                or window_width < 1 or window_height < 1:
            return None
        return [window_width, window_height]

    @classmethod
    def get_driver(cls):

        browser_type = get_config().browser.lower()
        browser_options = cls.accept_untrusted_certificates(browser_type)

        if browser_type == "firefox":
            driver = webdriver.Firefox(executable_path=firefox
                                       .GeckoDriverManager().install())
        elif browser_type in ["ie", "internet explorer"]:
            driver = webdriver.Ie(IEDriverManager().install())
        elif browser_type == "edge":
            driver = webdriver.Edge(EdgeChromiumDriverManager().install())
        else:
            driver = webdriver.Chrome(
                chrome_options=browser_options,
                executable_path=chrome.ChromeDriverManager().install()
            )

        resolution = cls.manage_resolution()
        if resolution is not None:
            driver.set_window_size(resolution[0], resolution[1])
        else:
            driver.maximize_window()
        return driver

    @staticmethod
    def wait(driver):
        return WebDriverWait(
            driver,
            15,
            ignored_exceptions=StaleElementReferenceException
        )
