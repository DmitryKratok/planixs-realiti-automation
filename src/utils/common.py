import os


def get_path_to_file(rel_path):
    """Helper method to get path to a file."""
    script_path = os.path.abspath(__file__)
    script_dir = os.path.split(script_path)[0]
    return os.path.join(script_dir, rel_path)


def get_root():
    root_path = os.path.dirname(
        os.path.abspath("AUTO_CONFIG")) or os.path.dirname(
        os.path.abspath("config.yaml"))
    return root_path
