import os
from datetime import datetime
from src.utils.config_manager import get_config


def get_screenshot_dir():
    screenshot_dir = get_config().screenshots.screenshots_folder
    if screenshot_dir:
        return screenshot_dir
    return os.environ.get("SCREENSHOT_DIR", None)


def take_screenshot(driver, node_id):
    """Make a screenshot with a name of the test, date and time"""
    file_name = f'{node_id}_' \
                f'{datetime.today().strftime("%Y-%m-%d_%H:%M")}.png'\
        .replace("/", "_").replace(":", "_")
    folder = get_screenshot_dir()
    file_path = f'{folder}/{file_name}'
    try:
        if not os.path.exists(folder):
            os.makedirs(folder)
        driver.save_screenshot(file_path)
    except Exception as e:
        print('Exception while screen-shot creation: {}'.format(e))
