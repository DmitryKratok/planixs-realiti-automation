"""Centralised configuration module"""

import yaml
import os.path
from pymlconf import ConfigManager

_config = None


def set_config(path=None):
    """Set up config using the file provided in `path`.

    :param path: The path to the config file to use. If not specified,
        `set_config()` will look in the AUTO_CONFIG environment variable.
    :raises: ValueError if no valid path is passed or found in AUTO_CONFIG.
    """
    global _config
    path = path or os.environ.get("AUTO_CONFIG", None)
    if path is None:
        raise ValueError(
            "No valid path found for configuration file. Either pass a path "
            "to set_config() or set the AUTO_CONFIG environment variable.")
    path = os.path.abspath(os.path.expanduser(path))
    _config = ConfigManager()
    _config.load_files(path)


def get_config():
    """Return the current application config object.

    :return: `ConfigManager`
    """
    global _config
    if _config is None:
        config_file = os.environ.get("AUTO_CONFIG", None)
        set_config(config_file)
    return _config


def get_yaml_data(yaml_path):
    with open(yaml_path, encoding='utf8') as yf:
        return yaml.load(yf, Loader=yaml.FullLoader)
