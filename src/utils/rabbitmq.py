import logging
import pika
from src.utils.yaml_helper import get_config


class RabbitRPCClient:
    """A class which acts as a Rabbit RPC Client."""
    __connection = None
    log = logging.getLogger(__name__)

    def __init__(self):
        """Initialise the rabbit client."""
        if RabbitRPCClient.__connection is None:
            try:
                params = get_config().rabbitmq
                credentials = pika.PlainCredentials(**params)
                parameters = pika.ConnectionParameters(
                    host='localhost',
                    port=5672,
                    credentials=credentials
                )
                RabbitRPCClient.__connection = pika. \
                    BlockingConnection(parameters)
                self.__channel = RabbitRPCClient.__connection.channel()
            except (Exception, ConnectionError) as error:
                self.log.error(error)

    def __call__(self, queue, message):
        """Make the call by sending a message"""
        self.__channel.basic_publish(exchange='', routing_key=queue,
                                     body=message)

    def send_message_to_queue(self, message, queue='boundary_router_input'):
        """Send a message to a desired queue
        (boundary_router_input by default). """
        self(queue, message)

    def close_connection(self):
        try:
            RabbitRPCClient.__connection.close()
        except (Exception, ConnectionError) as error:
            self.log.error(error)

    def wait_until_queue_is_empty(self, queue):
        """Waits until queue is empty.
        :param queue: queue name"""
        flag = True
        while flag:
            status = self.__channel.queue_declare(queue=queue, passive=True)
            flag = status.method.message_count != 0
